//
//  GGMainViewController.m
//  Мессенджер организаций
//
//  Created by Гусейн on 18.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//


//controllers
#import "GGMainViewController.h"
#import "GGAuthController.h"
#import "GGServiceAuthController.h"
#import "GGCompanyTabController.h"

@interface GGMainViewController ()

@end

@implementation GGMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    if ([[userDefaults objectForKey:@"Logged"] isEqualToString:@"Company"]) {
        GGCompanyTabController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GGCompanyTabController"];
        [self presentViewController:vc animated:YES completion:^{
            
        }];
        
    } if ([[userDefaults objectForKey:@"Logged"] isEqualToString:@"Client"]) {
        NSLog(@"Client");
        
    } else
    {
        NSLog(@"NOt logged ");
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)userButtonClicked:(UIButton *)sender {
    GGAuthController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GGAuthController"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)companyButtonClicked:(UIButton *)sender {
    GGServiceAuthController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GGServiceAuthController"];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
