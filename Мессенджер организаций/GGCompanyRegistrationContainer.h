//
//  GGCompanyRegistrationContainer.h
//  Мессенджер организаций
//
//  Created by Гусейн on 20.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CityChoosingController.h"

@interface GGCompanyRegistrationContainer : UITableViewController <СityChoosingDelegate>

@end
