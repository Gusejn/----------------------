//
//  GGSeviceUsersCell.m
//  Мессенджер организаций
//
//  Created by Гусейн on 27.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "GGSeviceUsersCell.h"

@implementation GGSeviceUsersCell


-(void) prepareForReuse
{
    self.userImageView.image = nil;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self roundMyView:self.userImageView borderRadius:23.f borderWidth:0.f color:nil];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)roundMyView:(UIView*)view
       borderRadius:(CGFloat)radius
        borderWidth:(CGFloat)border
              color:(UIColor*)color
{
    CALayer *layer = [view layer];
    layer.masksToBounds = YES;
    layer.cornerRadius = radius;
    layer.borderWidth = border;
    layer.borderColor = color.CGColor;
}

@end
