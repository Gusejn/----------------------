//
//  GGNotServiceUsersCell.h
//  Мессенджер организаций
//
//  Created by Гусейн on 27.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GGNotServiceUsersCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *addButton;

@end
