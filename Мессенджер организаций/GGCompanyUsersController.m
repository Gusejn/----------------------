//
//  GGCompanyUsersController.m
//  Мессенджер организаций
//
//  Created by Гусейн on 27.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "GGCompanyUsersController.h"
#import "GGAddUserToServiceController.h"

//cell
#import "GGSearchCell.h"
#import "GGSeviceUsersCell.h"

//models
#import "GGServiceUserModel.h"

//manager
#import "GGApiManager.h"

@interface GGCompanyUsersController () <UISearchBarDelegate>

@property (strong, nonatomic) NSMutableArray* usersArray;
@property (strong, nonatomic) NSArray* sortedArray;
@property (weak, nonatomic) IBOutlet UISearchBar *userSearchingSearchBar;

@end

static NSString* baseURL = @"http://balinasoft.com/messenger_organizations/uploads/img/";
static NSString* userCellIdentifier = @"GGSeviceUsersCell";
static NSString* searchCellIdentifier = @"GGSearchCell";

@implementation GGCompanyUsersController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.usersArray = [NSMutableArray array];
    [[GGApiManager sharedManager] getServiceUsersWithSuccess:^(id response) {
        NSArray* resultArray = [response objectForKey:@"data"];
        for (NSDictionary* objects in resultArray) {
            GGServiceUserModel* model = [[GGServiceUserModel alloc] initWithDictionary:objects];
            [self.usersArray addObject:model];
            
        }
        self.sortedArray = [NSArray arrayWithArray:self.usersArray];
        [self.tableView reloadData];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        
    }];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"GGSearchCell" bundle:nil]
         forCellReuseIdentifier:searchCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"GGSeviceUsersCell" bundle:nil]
         forCellReuseIdentifier:userCellIdentifier];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(addButton)];
    addButton.image = [UIImage imageNamed:@"plus_in_circle@3x.png"];
    self.navigationItem.rightBarButtonItem = addButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

#pragma mark - UITableViewDataSource

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return [NSString stringWithFormat:@"%lu" , (unsigned long)[self.sortedArray count]];
    }
    else return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    else {
        return [self.sortedArray count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        GGSearchCell* searchCell = [tableView dequeueReusableCellWithIdentifier:searchCellIdentifier];
        
        if (!searchCell) {
            searchCell = [[GGSearchCell alloc]
                          initWithStyle:UITableViewCellStyleDefault
                          reuseIdentifier:searchCellIdentifier];
        }
        
        [self configureSearchCell:searchCell atIndexPath:indexPath];
        return searchCell;
    } else {
        
        GGSeviceUsersCell* cell = [tableView dequeueReusableCellWithIdentifier:userCellIdentifier];
        if (!cell) {
            cell = [[GGSeviceUsersCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:userCellIdentifier];
        }
        [self configureServiceUsersCell:cell atIndexPath:indexPath];
        return cell;
        
    }
}

#pragma mark - Cells configurations

-(void)configureSearchCell:(GGSearchCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.userSearchingSearchBar.delegate = self;
    self.userSearchingSearchBar = cell.userSearchingSearchBar;
}

-(void)configureServiceUsersCell:(GGSeviceUsersCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    GGServiceUserModel* currUser = [self.sortedArray objectAtIndex:indexPath.row];
    cell.userNameLabel.text = currUser.userName;
    cell.userDiscountLabel.text = [NSString stringWithFormat:@"%@%%", currUser.subsDiscount];
    if (![currUser.userPhotoURL isEqualToString:@""]) {
        NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", baseURL, currUser.userPhotoURL]];
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        UIImage *profileImage = [UIImage imageWithData:imageData];
        cell.userImageView.image = profileImage;
    } else {
        cell.userImageView.image = [UIImage imageNamed:@"нет фото"];
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat result;
    if (indexPath.section == 0)
    {
        result = 44.f;
        
    } else {
        result = 63.f;
    }
    return result;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
}


- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSPredicate *predicate = [NSPredicate
                              predicateWithFormat:@"SELF.userName contains[cd] %@",searchText];
    
    self.sortedArray = [self.usersArray filteredArrayUsingPredicate:predicate];
    NSIndexSet* set = [[NSIndexSet alloc] initWithIndex:1];
    [self.tableView reloadSections:set withRowAnimation:UITableViewRowAnimationFade];

    if([searchText isEqualToString:@""])
    {
        self.sortedArray = self.usersArray;
        NSIndexSet* set = [[NSIndexSet alloc] initWithIndex:1];
        [self.tableView reloadSections:set withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - IBActions

-(void)addButton
{
    GGAddUserToServiceController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GGAddUserToServiceController"];
    [self.navigationController pushViewController:vc animated:YES];
}


@end

