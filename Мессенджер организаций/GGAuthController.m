//
//  GGAuthController.m
//  Мессенджер организаций
//
//  Created by Гусейн on 19.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "GGAuthController.h"
#import "GGClientRegistrationController.h"
#import "GGApiManager.h"
#import "GGCompanyTabController.h"
@interface GGAuthController ()

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *registrationButton;

- (IBAction)registrationPressed:(UIButton *)sender;
- (IBAction)facebookPressed:(UIButton *)sender;
- (IBAction)vkPressed:(UIButton *)sender;

@end

@implementation GGAuthController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self setBottomLineWithTextField:self.loginTextField];
    [self setBottomLineWithTextField:self.passwordTextField];
    [self setBottomLineWithButton:_registrationButton];
    [self setPlaceholderForTextFields];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark Bottom line

-(void)setBottomLineWithTextField:(UITextField *)textField
{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.5;
    border.borderColor = [UIColor whiteColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
}

-(void)setBottomLineWithButton:(UIButton *)button
{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.5;
    border.borderColor = [UIColor whiteColor].CGColor;
    border.frame = CGRectMake(4, button.frame.size.height - borderWidth, button.frame.size.width - 7, button.frame.size.height);
    border.borderWidth = borderWidth;
    [button.layer addSublayer:border];
    button.layer.masksToBounds = YES;
}

-(void)setPlaceholderForTextFields
{
    NSAttributedString *loginTextField = [[NSAttributedString alloc] initWithString:self.loginTextField.placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    NSAttributedString *passwordTextField = [[NSAttributedString alloc] initWithString:self.passwordTextField.placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    self.loginTextField.attributedPlaceholder = loginTextField;
    self.passwordTextField.attributedPlaceholder = passwordTextField;
}
#pragma mark UITextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
    
}
#pragma mark - IBActions
- (IBAction)registrationPressed:(UIButton *)sender {
    GGClientRegistrationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GGClientRegistrationController"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)enterAction:(UIButton *)sender {
    if(!([self.loginTextField.text isEqualToString:@""] |
         [self.passwordTextField.text isEqualToString:@""]))
    {
        if ([self.loginTextField.text containsString:@"@"]) {
            [[GGApiManager sharedManager] authorizeWithEmail:self.loginTextField.text
                                                        type:@"Client"
                                                    password:self.passwordTextField.text
                                                       token:@""
                                                   onSuccess:^(NSString *status) {
//                                                       GGCompanyTabController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GGCompanyTabController"];
//                                                       [self presentViewController:vc animated:YES completion:^{
//                                                           
//                                                       }];

                                                       
                                                   }
                                                   onFailure:^(NSError *error, NSInteger statusCode) {
                                                       
                                                   }];
            
        } else {
            [[GGApiManager sharedManager] authorizeWithPhone:self.loginTextField.text
                                                        type:@"Client"
                                                    password:self.passwordTextField.text
                                                       token:@""
                                                   onSuccess:^(NSString *status) {
//                                                       GGCompanyTabController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GGCompanyTabController"];
//                                                       [self presentViewController:vc animated:YES completion:^{
//                                                           
//                                                       }];

                                                       
                                                   }
                                                   onFailure:^(NSError *error, NSInteger statusCode) {
                                                       
                                                   }];
        }
        
        
    }
    
}

- (IBAction)facebookPressed:(UIButton *)sender {
}

- (IBAction)vkPressed:(UIButton *)sender {
}

@end
