//
//  GGClientRegistrationContainer.m
//  Мессенджер организаций
//
//  Created by Гусейн on 20.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "GGClientRegistrationContainer.h"
#import "GGApiManager.h"

//controllers
#import "GGCompanyTabController.h"

@interface GGClientRegistrationContainer ()
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *birthdayTextField;
@property (weak, nonatomic) IBOutlet UITextField *genderTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UIButton *enterButton;

@property (strong, nonatomic) NSString* country;
@property (strong, nonatomic) NSString* city;
@property (assign, nonatomic) NSInteger countryID;
@property (assign, nonatomic) NSInteger cityID;


@end

@implementation GGClientRegistrationContainer

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self setBottomLineWithTextField:self.nameTextField];
    [self setBottomLineWithTextField:self.cityTextField];
    [self setBottomLineWithTextField:self.phoneNumberTextField];
    [self setBottomLineWithTextField:self.emailTextField];
    [self setBottomLineWithTextField:self.birthdayTextField];
    [self setBottomLineWithTextField:self.genderTextField];
    [self setBottomLineWithTextField:self.passwordTextField];
    [self setBottomLineWithButton:self.enterButton];
    [self setPlaceholderForTextFields];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Bottom line

-(void)setPlaceholderForTextFields
{
    NSAttributedString *nameTextField = [[NSAttributedString alloc] initWithString:self.nameTextField.placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    NSAttributedString *phoneNumberTextField = [[NSAttributedString alloc] initWithString:self.phoneNumberTextField.placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    NSAttributedString *emailTextField = [[NSAttributedString alloc] initWithString:self.emailTextField.placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    NSAttributedString *cityTextField = [[NSAttributedString alloc] initWithString:self.cityTextField.placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    NSAttributedString *passwordTextField = [[NSAttributedString alloc] initWithString:self.passwordTextField.placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    NSAttributedString *birthdayTextField = [[NSAttributedString alloc] initWithString:self.birthdayTextField.placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    NSAttributedString *genderTextField = [[NSAttributedString alloc] initWithString:self.genderTextField.placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    
    self.birthdayTextField.attributedPlaceholder = birthdayTextField;
    self.genderTextField.attributedPlaceholder = genderTextField;
    self.nameTextField.attributedPlaceholder = nameTextField;
    self.phoneNumberTextField.attributedPlaceholder = phoneNumberTextField;
    self.emailTextField.attributedPlaceholder = emailTextField;
    self.cityTextField.attributedPlaceholder = cityTextField;
    self.passwordTextField.attributedPlaceholder = passwordTextField;
}


-(void)setBottomLineWithTextField:(UITextField *)textField
{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.5;
    border.borderColor = [UIColor whiteColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
}

-(void)setBottomLineWithButton:(UIButton *)button
{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.5;
    border.borderColor = [UIColor whiteColor].CGColor;
    border.frame = CGRectMake(4, button.frame.size.height - borderWidth, button.frame.size.width - 7, button.frame.size.height);
    border.borderWidth = borderWidth;
    [button.layer addSublayer:border];
    button.layer.masksToBounds = YES;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if ([textField isEqual:self.cityTextField]) {
        return NO;
    } else {
        return YES;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
    
}

#pragma mark - IBActions


- (IBAction)cityTextFieldTouchDown:(UITextField *)sender {
    CityChoosingController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CityChoosingController"];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)cityForAccDidSelectedWithDictionary:(NSDictionary*)dictionary
{
    self.cityTextField.text = [dictionary objectForKey:@"cityText"];
    self.country = [dictionary objectForKey:@"countryText"];
    self.cityID = [[dictionary objectForKey:@"cityID"] integerValue];
    self.countryID = [[dictionary objectForKey:@"CountryID"] integerValue];
}

- (IBAction)registrationAction:(UIButton *)sender {
    if(!([self.nameTextField.text isEqualToString:@""] |
         [self.phoneNumberTextField.text isEqualToString:@""] |
         [self.emailTextField.text isEqualToString:@""] |
         [self.cityTextField.text isEqualToString:@""]))
    {
        NSInteger genderID;
        if([self.genderTextField.text isEqualToString:@"Мужской"])
        {
            genderID = 1;
        } else
        {
            genderID = 2;
        }
        NSDictionary* params = @{@"user_name":self.nameTextField.text,
                                 @"user_city":self.cityTextField.text,
                                 @"user_phone_number":self.phoneNumberTextField.text,
                                 @"user_mail":self.emailTextField.text,
                                 @"user_password":self.passwordTextField.text,
                                 @"user_country":self.country,
                                 @"user_sex":@(genderID),
                                 @"user_date_birthday":self.birthdayTextField.text,
                                 @"user_country_id":@(self.countryID),
                                 @"user_city_id":@(self.cityID)};
        [[GGApiManager sharedManager] registrateClientWithParams:params
                                                        onSuccess:^(NSString *status) {
//                                                            GGCompanyTabController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GGCompanyTabController"];
//                                                            [self presentViewController:vc animated:YES completion:^{
//                                                                
//                                                            }];
                                                            
                                                        }
                                                        onFailure:^(NSError *error, NSInteger statusCode) {
                                                            
                                                        }];
    }
}

- (IBAction)enterButton:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
