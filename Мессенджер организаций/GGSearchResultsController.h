//
//  GGSearchResultsController.h
//  Мессенджер организаций
//
//  Created by Гусейн on 21.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GGSearchDelegate.h"
@interface GGSearchResultsController : UITableViewController

@property (strong, nonatomic) UISearchController* searchController;
@property (nonatomic, assign)   id<GGSearchDelegate> delegate;
@property (strong, nonatomic) NSString* selectedSearchingType;
@property (assign, nonatomic) NSInteger countryID;
@property (nonatomic, strong) NSMutableArray *resultsArray;

-(void)searchCityWithText:(NSString*)text andOffset:(NSInteger)offset andCount:(NSInteger)count;
-(void)searchCountries:(NSInteger)offset andCount:(NSInteger)count;

@end
