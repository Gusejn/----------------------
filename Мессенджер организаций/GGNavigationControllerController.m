//
//  GGNavigationControllerController.m
//  Мессенджер организаций
//
//  Created by Гусейн on 18.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "GGNavigationControllerController.h"

@interface GGNavigationControllerController ()

@end

@implementation GGNavigationControllerController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];

    [self updateNavigationController];
}

- (void)updateNavigationController {
    
    [self.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage = [UIImage new];
    self.navigationBar.translucent = YES;
    [self.navigationBar setTintColor:[UIColor whiteColor]];
}


@end
