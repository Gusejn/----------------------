//
//  GGCompanySettingsController.m
//  Мессенджер организаций
//
//  Created by Гусейн on 20.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "GGCompanySettingsController.h"
#import "GGApiManager.h"
#import "NYAlertViewController.h"
#import "GGCompanyUsersController.h"

@interface GGCompanySettingsController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *workTimeTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UISwitch *pushSwitch;

@property (strong, nonatomic) NSString* country;
@property (strong, nonatomic) NSString* city;
@property (assign, nonatomic) NSInteger countryID;
@property (assign, nonatomic) NSInteger cityID;
@property (strong, nonatomic) NSString* photoUrlString;

@end

static NSString* baseURL = @"http://balinasoft.com/messenger_organizations/uploads/img/";

@implementation GGCompanySettingsController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self roundMyView:self.photoImageView borderRadius:37.f borderWidth:0.f color:nil];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:77/255.f
                                                                             green:169/255.f
                                                                              blue:250/255.f
                                                                             alpha:1.0]];
    self.navigationController.navigationBar.translucent = NO;
    [[GGApiManager sharedManager] getUserInfoWithSuccess:^(id response) {
        NSDictionary* result = [response objectForKey:@"data"];
        self.city = [result objectForKey:@"user_city"];
        self.cityID = [[result objectForKey:@"user_city_id"] integerValue];
        self.country = [result objectForKey:@"user_country"];
        self.countryID = [[result objectForKey:@"user_country_id"] integerValue];
        self.cityTextField.text = _city;
        self.nameTextField.text = [result objectForKey:@"user_name"];
        self.addressTextField.text = [result objectForKey:@"user_address"];
        self.phoneTextField.text = [result objectForKey:@"user_phone_number"];
        self.workTimeTextField.text = [result objectForKey:@"user_working_hours"];
        if (![[result objectForKey:@"user_photo_url"] isEqualToString:@""]) {
            NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", baseURL, [result objectForKey:@"user_photo_url"]]];
            NSData *imageData = [NSData dataWithContentsOfURL:url];
            self.photoUrlString = [result objectForKey:@"user_photo_url"];
            UIImage *profileImage = [UIImage imageWithData:imageData];
            self.photoImageView.image = profileImage;
        } else {
            self.photoImageView.image = [UIImage imageNamed:@"нет фото"];
        }
        
        
        if ([[result objectForKey:@"user_push_on"] integerValue] == 1) {
            self.pushSwitch.on = YES;
        } else {
            self.pushSwitch.on = NO;
        }
        
    }
                                               onFailure:^(NSError *error, NSInteger statusCode) {
                                                   
                                               }];
    

    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:77/255.f
                                                                             green:169/255.f
                                                                              blue:250/255.f
                                                                             alpha:1.0]];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImage)];
    singleTap.numberOfTapsRequired = 1;
    self.photoImageView.userInteractionEnabled = YES;
    [self.photoImageView addGestureRecognizer:singleTap];
    
    self.navigationController.navigationBar.translucent = NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:77/255.f
                                                                             green:169/255.f
                                                                              blue:250/255.f
                                                                             alpha:1.0]];
    self.navigationController.navigationBar.translucent = NO;
    [[GGApiManager sharedManager] getUserInfoWithSuccess:^(id response) {
        NSDictionary* result = [response objectForKey:@"data"];
        self.city = [result objectForKey:@"user_city"];
        self.cityID = [[result objectForKey:@"user_city_id"] integerValue];
        self.country = [result objectForKey:@"user_country"];
        self.countryID = [[result objectForKey:@"user_country_id"] integerValue];
        self.cityTextField.text = _city;
        self.nameTextField.text = [result objectForKey:@"user_name"];
        self.addressTextField.text = [result objectForKey:@"user_address"];
        self.phoneTextField.text = [result objectForKey:@"user_phone_number"];
        self.workTimeTextField.text = [result objectForKey:@"user_working_hours"];
        if ([[result objectForKey:@"user_push_on"] integerValue] == 1) {
            self.pushSwitch.on = YES;
        } else {
            self.pushSwitch.on = NO;
        }
        
    }
                                               onFailure:^(NSError *error, NSInteger statusCode) {
                                                   
                                               }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if ([textField isEqual:self.cityTextField]) {
        return NO;
    } else {
        return YES;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
    
}

#pragma mark -ImageController 

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    NSData *imageData = UIImagePNGRepresentation(chosenImage);

    self.photoImageView.image = chosenImage;
    [[GGApiManager sharedManager] uploadImageForUser:imageData onSuccess:^(NSString* response) {
        self.photoUrlString = [response substringFromIndex:58];
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        
    }];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)takePhoto {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}

- (void)selectPhoto {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}

#pragma mark - IBActions

- (void)tapImage {
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"Выберите действие";
    alertViewController.message = @"";
    alertViewController.buttonCornerRadius = 20.0f;
    alertViewController.view.tintColor = self.view.tintColor;
    
    alertViewController.titleFont = [UIFont fontWithName:@"AvenirNext-Bold" size:19.0f];
    alertViewController.messageFont = [UIFont fontWithName:@"AvenirNext-Medium" size:16.0f];
    alertViewController.buttonTitleFont = [UIFont fontWithName:@"AvenirNext-Regular" size:14.0f];
    alertViewController.cancelButtonTitleFont = [UIFont fontWithName:@"AvenirNext-Medium" size:alertViewController.cancelButtonTitleFont.pointSize];
    
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:NSLocalizedString(@"Выбрать из галереи", nil)
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES
                                                                                       completion:^{
                                                                                           [self selectPhoto];

                                                                                       }];
                                                              
                                                          }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:NSLocalizedString(@"Сделать фото", nil)
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES
                                                                                       completion:^{
                                                                                           [self takePhoto];

                                                                                       }];
                                                              
                                                          }]];
    [self presentViewController:alertViewController animated:YES completion:nil];
    
}


- (IBAction)cityTextFieldTouchDown:(UITextField *)sender {
    CityChoosingController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CityChoosingController"];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)cityForAccDidSelectedWithDictionary:(NSDictionary*)dictionary
{
    self.cityTextField.text = [dictionary objectForKey:@"cityText"];
    self.country = [dictionary objectForKey:@"countryText"];
    self.cityID = [[dictionary objectForKey:@"cityID"] integerValue];
    self.countryID = [[dictionary objectForKey:@"CountryID"] integerValue];
}

- (IBAction)updateAction:(UIButton *)sender {
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSString* deviceID = [userDefaults objectForKey:@"device_uid"];
    NSDictionary* params = @{@"device_uid":deviceID,
                             @"user_country_id":[self ObjectOrNull:@(self.countryID)],
                             @"user_city_id":[self ObjectOrNull:@(self.cityID)],
                             @"user_city":[self ObjectOrNull:self.city],
                             @"user_country":[self ObjectOrNull:self.country],
                             @"user_name":[self ObjectOrNull:self.nameTextField.text],
                             @"user_address":[self ObjectOrNull:self.addressTextField.text],
                             @"user_push_on":[self ObjectOrNull:@(1)],
                             @"user_working_hours":[self ObjectOrNull:self.workTimeTextField.text],
                             @"user_phone_number":[self ObjectOrNull:self.phoneTextField.text],
                             @"user_photo_url":[self ObjectOrNull:self.photoUrlString]};
    
    [[GGApiManager sharedManager] updateInformationWithParams:params
                                                    onSuccess:^(id response) {
                                                        
                                                    }
                                                    onFailure:^(NSError *error, NSInteger statusCode) {
                                                        
                                                    }];
}

- (IBAction)logoutAction:(UIButton *)sender {
    [[GGApiManager sharedManager] exitWithSuccess:^(id response) {
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@"Logout" forKey:@"Logged"];
        [userDefaults synchronize];
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
        
    }
                                        onFailure:^(NSError *error, NSInteger statusCode) {
                                            
                                        }];
    
}

- (IBAction)companyUsersTapped:(UIBarButtonItem *)sender {
    GGCompanyUsersController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GGCompanyUsersController"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)roundMyView:(UIView*)view
       borderRadius:(CGFloat)radius
        borderWidth:(CGFloat)border
              color:(UIColor*)color
{
    CALayer *layer = [view layer];
    layer.masksToBounds = YES;
    layer.cornerRadius = radius;
    layer.borderWidth = border;
    layer.borderColor = color.CGColor;
}

-(id) ObjectOrNull: (id) object
{
    return object ?: [NSNull null];
}

@end
