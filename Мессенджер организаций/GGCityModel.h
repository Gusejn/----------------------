//
//  GGCityModel.h
//  Мессенджер организаций
//
//  Created by Гусейн on 21.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GGCityModel : NSObject

@property (strong, nonatomic) NSString* name;
@property (assign, nonatomic) NSInteger ID;

-(id)initWithName:(NSString*)name andID:(NSInteger)ID;

@end
