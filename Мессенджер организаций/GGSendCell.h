//
//  GGSendCell.h
//  Мессенджер организаций
//
//  Created by Гусейн on 28.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GGSendCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@end
