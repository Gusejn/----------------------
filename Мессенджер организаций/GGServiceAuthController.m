//
//  GGServiceAuthController.m
//  Мессенджер организаций
//
//  Created by Гусейн on 19.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "GGServiceAuthController.h"
#import "GGCompanyRegistrationController.h"
#import "GGCompanyTabController.h"
#import "GGApiManager.h"

@interface GGServiceAuthController ()

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

@implementation GGServiceAuthController

- (void)viewDidLoad {
    [super viewDidLoad];
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#pragma mark - IBActions
- (IBAction)registrationPressed:(UIButton *)sender {
    GGCompanyRegistrationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GGCompanyRegistrationController"];
    [self.navigationController pushViewController:vc animated:YES];
}


- (IBAction)enterAction:(UIButton *)sender {
    if(!([self.loginTextField.text isEqualToString:@""] |
       [self.passwordTextField.text isEqualToString:@""]))
    {
        if ([self.loginTextField.text containsString:@"@"]) {
            [[GGApiManager sharedManager] authorizeWithEmail:self.loginTextField.text
                                                        type:@"Company"
                                                    password:self.passwordTextField.text
                                                       token:@""
                                                   onSuccess:^(NSString *status) {
                                                       GGCompanyTabController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GGCompanyTabController"];
                                                       [self presentViewController:vc animated:YES completion:^{
                                                           
                                                       }];
                                                   }
                                                   onFailure:^(NSError *error, NSInteger statusCode) {
                                                       
                                                   }];
 
        } else {
        [[GGApiManager sharedManager] authorizeWithPhone:self.loginTextField.text
                                                    type:@"Company"
                                                password:self.passwordTextField.text
                                                   token:@""
                                               onSuccess:^(NSString *status) {
                                                   GGCompanyTabController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GGCompanyTabController"];
                                                   [self presentViewController:vc animated:YES completion:^{
                                                       
                                                   }];
                                               }
                                               onFailure:^(NSError *error, NSInteger statusCode) {
                                                   
                                               }];
        }
    }

    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
    
}

- (IBAction)facebookPressed:(UIButton *)sender {
}

- (IBAction)vkPressed:(UIButton *)sender {
}

@end
