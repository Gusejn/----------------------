//
//  GGCityModel.m
//  Мессенджер организаций
//
//  Created by Гусейн on 21.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "GGCityModel.h"

@implementation GGCityModel

-(id)initWithName:(NSString*)name andID:(NSInteger)ID
{
    self = [super init];
    if (self) {
        self.name = name;
        self.ID = ID;
    }
    return self;
}
@end
