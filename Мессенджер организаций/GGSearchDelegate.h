//
//  GGSearchDelegate.h
//  Мессенджер организаций
//
//  Created by Гусейн on 21.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GGCityModel.h"
#import "GGCountryModel.h"

@protocol GGSearchDelegate <NSObject>

@optional
-(void)searchControllerDidDissmissWithCountry:(GGCountryModel *)country;
-(void)searchControllerDidDissmissWithCity:(GGCityModel *)city;
@end

@interface GGSearchDelegate : NSObject

@end
