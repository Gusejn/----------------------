//
//  CityChoosingController.h
//  Мессенджер организаций
//
//  Created by Гусейн on 21.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol СityChoosingDelegate;

@interface CityChoosingController : UIViewController

@property (nonatomic, weak) id <СityChoosingDelegate> delegate;

@end

@protocol СityChoosingDelegate <NSObject>
@required
- (void)cityForAccDidSelectedWithDictionary:(NSDictionary*)dictionary;
@end