//
//  GGApiManager.m
//  Мессенджер организаций
//
//  Created by Гусейн on 20.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "GGApiManager.h"
#import "SVProgressHUD.h"
#import <MagicalRecord/MagicalRecord.h>
#import "GGCityModel.h"
#import "GGCountryModel.h"

#define BOUNDARY @"http://balinasoft.com/messenger_organizations/upload/image"
@implementation GGApiManager


+(GGApiManager*) sharedManager
{    static GGApiManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[GGApiManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://balinasoft.com/messenger_organizations"] sessionConfiguration:nil];
    });
    return manager;
}

#pragma mark - Authorization
-(void) authorizeWithEmail:(NSString*)Email
                      type:(NSString*)authType
                  password:(NSString*)password
                     token:(NSString*)token
                 onSuccess:(void(^)(NSString* status))success
                 onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{
    NSDictionary *params = @{@"user_mail":[self ObjectOrNull:Email],
                             @"user_password":[self ObjectOrNull:password],
                             @"device_token":[self ObjectOrNull:token]};
    
    [self POST:[NSString stringWithFormat:@"%@/auth/login/1", self.baseURL]
    parameters:params
      progress:^(NSProgress * _Nonnull uploadProgress) {
          
      }
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           NSString* result = [responseObject objectForKey:@"status"];
           NSLog(@"%@", responseObject);
           NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
           [userDefaults setObject:[[responseObject objectForKey:@"data"] objectForKey:@"device_uid"] forKey:@"device_uid"];
           [userDefaults setObject:authType forKey:@"Logged"];
           [userDefaults synchronize];
           if (success) {
               success(result);
           }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
           });
       }];
}

-(void) authorizeWithPhone:(NSString*)Phone
                      type:(NSString*)authType
                  password:(NSString*)password
                     token:(NSString*)token
                 onSuccess:(void(^)(NSString* status))success
                 onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{
    NSDictionary *params = @{@"user_phone_number":[self ObjectOrNull:Phone],
                             @"user_password":[self ObjectOrNull:password],
                             @"device_token":[self ObjectOrNull:token]};
    
    [self POST:[NSString stringWithFormat:@"%@/auth/login/1", self.baseURL]
    parameters:params
      progress:^(NSProgress * _Nonnull uploadProgress) {
          
      }
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           NSString* result = [responseObject objectForKey:@"status"];
           NSLog(@"%@", responseObject);
           NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
           [userDefaults setObject:[[responseObject objectForKey:@"data"] objectForKey:@"device_uid"] forKey:@"device_uid"];
           [userDefaults setObject:authType forKey:@"Logged"];
           [userDefaults synchronize];
           NSLog(@"%@",[[responseObject objectForKey:@"data"] objectForKey:@"device_uid"]);
           if (success) {
               success(result);
           }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
           });
       }];
}

-(void) authorizeWithSocialID:(NSString*)Phone
                         type:(NSString*)authType
                     password:(NSString*)password
                        token:(NSString*)token
                    onSuccess:(void(^)(NSString* status))success
                    onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{
    NSDictionary *params = @{@"user_id_soc":[self ObjectOrNull:Phone],
                             @"device_token":[self ObjectOrNull:token]};
    
    [self POST:[NSString stringWithFormat:@"%@/auth/login/1", self.baseURL]
    parameters:params
      progress:^(NSProgress * _Nonnull uploadProgress) {
          
      }
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           NSString* result = [responseObject objectForKey:@"status"];
           NSLog(@"%@", responseObject);
           NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
           [userDefaults setObject:[[responseObject objectForKey:@"data"] objectForKey:@"device_uid"] forKey:@"device_uid"];
           [userDefaults setObject:authType forKey:@"Logged"];
           [userDefaults synchronize];
           if (success) {
               
               success(result);
           }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
           });
       }];
}

#pragma mark - Registration
-(void) registrateCompanyWithParams:(NSDictionary*)params
                          onSuccess:(void(^)(NSString* status))success
                          onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{
    [self POST:[NSString stringWithFormat:@"%@/auth/registration/1/2", self.baseURL]
    parameters:params
      progress:^(NSProgress * _Nonnull uploadProgress) {
          
      }
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           NSString* result = [responseObject objectForKey:@"status"];
           NSLog(@"%@", responseObject);
           if (success) {
               success(result);
               NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
               [userDefaults setObject:[[responseObject objectForKey:@"data"] objectForKey:@"device_uid"] forKey:@"device_uid"];
               [userDefaults synchronize];
               [self authorizeWithEmail:[self ObjectOrNull:[params objectForKey:@"user_mail"]]
                                   type:@"Company"
                               password:[self ObjectOrNull:[params objectForKey:@"user_password"]]
                                  token:[self ObjectOrNull:[params objectForKey:@"device_token"]]
                              onSuccess:^(NSString *status) {
                                  
                              }
                              onFailure:^(NSError *error, NSInteger statusCode) {
                                  
                              }];
           }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
           });
       }];
    
}

-(void) registrateClientWithParams:(NSDictionary*)params
                         onSuccess:(void(^)(NSString* status))success
                         onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{
    [self POST:[NSString stringWithFormat:@"%@/auth/registration/1/1", self.baseURL]
    parameters:params
      progress:^(NSProgress * _Nonnull uploadProgress) {
          
      }
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           NSString* result = [responseObject objectForKey:@"status"];
           NSLog(@"%@", responseObject);
           if (success) {
               success(result);
               NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
               [userDefaults setObject:[[responseObject objectForKey:@"data"] objectForKey:@"device_uid"] forKey:@"device_uid"];
               [userDefaults synchronize];
               [self authorizeWithEmail:[self ObjectOrNull:[params objectForKey:@"user_mail"]]
                                   type:@"Client"
                               password:[self ObjectOrNull:[params objectForKey:@"user_password"]]
                                  token:[self ObjectOrNull:[params objectForKey:@"device_token"]]
                              onSuccess:^(NSString *status) {
                                  
                              }
                              onFailure:^(NSError *error, NSInteger statusCode) {
                                  
                              }];
           }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
           });
       }];
    
}


#pragma mark - Get cities from VK

-(void)getCountriesWithOffset:(NSInteger)offset
                        count:(NSInteger)count
                    onSuccess:(void(^)(id response))success
                    onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;
{
    NSDictionary* params = @{@"count":@(count),
                             @"offset":@(offset),
                             @"need_all":@(0)};
    [self GET:@"https://api.vk.com/method/database.getCountries"
   parameters:params
     progress:^(NSProgress * _Nonnull downloadProgress) {
         
     }
      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          
          if (success) {
              success(responseObject);
          }
      }
      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          
      }];

    
}

-(void)getCitiesWithText:(NSString*)text
                  offset:(NSInteger)offset
                   count:(NSInteger)count
               countryID:(NSInteger)countryID
               onSuccess:(void(^)(id response))success
               onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{
    NSDictionary* params = @{@"q":text,
                             @"count":@(count),
                             @"offset":@(offset),
                             @"need_all":@(1),
                             @"country_id":@(countryID)};
    [self GET:@"https://api.vk.com/method/database.getCities"
   parameters:params
     progress:^(NSProgress * _Nonnull downloadProgress) {
         
     }
      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          if (success) {
              success(responseObject);
          }
      }
      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          
      }];
}

#pragma mark - Exit 

-(void)exitWithSuccess:(void(^)(id response))success
              onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSString* deviceID = [userDefaults objectForKey:@"device_uid"];
    NSDictionary* params = @{@"device_uid":deviceID};
    [self POST:[NSString stringWithFormat:@"%@/auth/logout", self.baseURL]
    parameters:params
      progress:^(NSProgress * _Nonnull uploadProgress) {
          
      }
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           if (success) {
               success(responseObject);
           }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
           });
       }];
}

#pragma mark - get Users(for Service)

-(void)getServiceUsersWithSuccess:(void(^)(id response))success
                        onFailure: (void(^)(NSError* error, NSInteger statusCode))failure {
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSString* deviceID = [userDefaults objectForKey:@"device_uid"];
    NSDictionary* params = @{@"device_uid":deviceID};
    
    [self POST:[NSString stringWithFormat:@"%@/subscription/list/1", self.baseURL]
    parameters:params
      progress:^(NSProgress * _Nonnull uploadProgress) {
          
      }
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           if (success) {
               success(responseObject);
           }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           dispatch_async(dispatch_get_main_queue(), ^{
               NSLog(@"%@",[error.userInfo objectForKey:@"com.alamofire.serialization.response.error.response"]);
               [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
           });
       }];

    
}

-(void)getAllUsersWithSuccess:(void(^)(id response))success
                   onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSString* deviceID = [userDefaults objectForKey:@"device_uid"];
    NSDictionary* params = @{@"device_uid":deviceID};
    [self POST:[NSString stringWithFormat:@"%@/subscription/list/2", self.baseURL]
    parameters:params
      progress:^(NSProgress * _Nonnull uploadProgress) {
          
      }
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           if (success) {
               success(responseObject);
           }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
           });
       }];
    
}

#pragma mark Get User Info

-(void)getUserInfoWithSuccess:(void(^)(id response))success
                    onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSString* deviceID = [userDefaults objectForKey:@"device_uid"];
    NSDictionary* params = @{@"device_uid":deviceID};
    [self POST:[NSString stringWithFormat:@"%@/profile/get", self.baseURL]
    parameters:params
      progress:^(NSProgress * _Nonnull uploadProgress) {
          
      }
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           if (success) {
               success(responseObject);
           }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
           });
       }];
}



#pragma mark UpdateUserInfo

-(void)updateInformationWithParams:(NSDictionary*)params
                                onSuccess:(void(^)(id response))success
                                onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{
    [self POST:[NSString stringWithFormat:@"%@/profile/update/2", self.baseURL]
    parameters:params
      progress:^(NSProgress * _Nonnull uploadProgress) {
          
      }
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [SVProgressHUD showInfoWithStatus:@"OK!"];
           });
           if (success) {
               success(responseObject);
           }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
           });
       }];

}

-(void)uploadImageForUser:(NSData*)image
                onSuccess:(void(^)(NSString* response))success
                onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{
    // creating a NSMutableURLRequest that we can manipulate before sending
    NSString *urlString = [NSString stringWithFormat:@"%@/upload/image", self.baseURL];
    NSString *filename = @"filename";
    NSMutableURLRequest* request= [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=image; filename=\"%@.jpg\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[NSData dataWithData:image]];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:postbody];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    id json = [NSJSONSerialization JSONObjectWithData:returnData options:0 error:nil];
    NSString* returnedString = [ NSString stringWithFormat:@"%@" ,[[json objectForKey:@"data"] objectForKey:@"full_url_image"]];
    if (success) {
        success(returnedString);
    }
}

// profile/update/2

#pragma mark objectOrNull
-(id) ObjectOrNull: (id) object
{
    return object ?: [NSNull null];
}

#pragma mark - Working with invitations 

-(void)sendInvitationWithSubscriptionID:(NSMutableString*)ID
                              onSuccess:(void(^)(NSString* response))success
                              onFailure: (void(^)(NSError* error, NSInteger statusCode))failure
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSString* deviceID = [userDefaults objectForKey:@"device_uid"];
    NSDictionary* params = @{@"device_uid":deviceID,
                             @"subscription_user_id":ID};
    [self POST:[NSString stringWithFormat:@"%@/subscription/add", self.baseURL]
    parameters:params
      progress:^(NSProgress * _Nonnull uploadProgress) {
          
      }
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           if (success) {
               success(responseObject);
           }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
           });
       }];

}


@end
