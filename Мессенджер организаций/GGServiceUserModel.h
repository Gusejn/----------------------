//
//  GGServiceUserModel.h
//  Мессенджер организаций
//
//  Created by Гусейн on 27.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GGServiceUserModel : NSObject

@property (strong,nonatomic) NSString* user_id;
@property (strong,nonatomic) NSString* userName;
@property (strong, nonatomic) NSString* userPhotoURL;
@property (strong, nonatomic) NSString* subsDiscount;

-(id)initWithDictionary:(NSDictionary*) otherDictionary;

@end
