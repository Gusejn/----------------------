//
//  GGAddUserToServiceController.m
//  Мессенджер организаций
//
//  Created by Гусейн on 27.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "GGAddUserToServiceController.h"
#import "GGAddUserToServiceController.h"

//cell
#import "GGNotServiceUsersCell.h"
#import "GGSendCell.h"

//models
#import "GGNotSeviceUserModel.h"

//manager
#import "GGApiManager.h"

@interface GGAddUserToServiceController () <UISearchBarDelegate>

@property (strong, nonatomic) NSMutableArray* usersArray;
@property (strong, nonatomic) NSMutableArray* invitedArray;
@property (strong, nonatomic) NSMutableArray* cellsArray;
@property (strong, nonatomic) NSArray* sortedArray;

@property (assign, nonatomic) BOOL isClickedAllMembers;

@end

static NSString* baseURL = @"http://balinasoft.com/messenger_organizations/uploads/img/";
static NSString* userCellIdentifier = @"GGNotServiceUsersCell";
static NSString* sendCellIdentifier = @"GGSendCell";

@implementation GGAddUserToServiceController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isClickedAllMembers = NO;
    //adding barButtons
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(addButton)];
    addButton.image = [UIImage imageNamed:@"tick@3x.png"];
    self.navigationItem.rightBarButtonItem = addButton;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(backButton)];
    backButton.image = [UIImage imageNamed:@"arrow@3x.png"];
    self.navigationItem.leftBarButtonItem = backButton;
    
    
    //setting searchBar
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(-5.0, 0.0, 320.0, 44.0)];
    searchBar.searchBarStyle = UISearchBarStyleMinimal;
    searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    UIView *searchBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 310.0, 44.0)];
    searchBarView.autoresizingMask = 0;
    searchBar.delegate = self;
    [searchBarView addSubview:searchBar];
    self.navigationItem.titleView = searchBarView;
    
    //init. arrays
    self.invitedArray = [NSMutableArray array];
    self.usersArray = [NSMutableArray array];
    self.cellsArray = [NSMutableArray array];
    
    //register cells
    [self.tableView registerNib:[UINib nibWithNibName:@"GGNotServiceUsersCell" bundle:nil]
         forCellReuseIdentifier:userCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"GGSendCell" bundle:nil]
         forCellReuseIdentifier:sendCellIdentifier];
    
    //getting users
    [[GGApiManager sharedManager] getAllUsersWithSuccess:^(id response) {
        NSArray* resultArray = [response objectForKey:@"data"];
        for (NSDictionary* objects in resultArray) {
            GGNotSeviceUserModel* model = [[GGNotSeviceUserModel alloc] initWithDictionary:objects];
            [self.usersArray addObject:model];
        }
        self.sortedArray = [NSArray arrayWithArray:self.usersArray];
        [self.tableView reloadData];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - UITableViewDataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        return [self.sortedArray count];
    } else return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     if (indexPath.section == 0) {
    GGNotServiceUsersCell* cell = [tableView dequeueReusableCellWithIdentifier:userCellIdentifier];
    if (!cell) {
        cell = [[GGNotServiceUsersCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:userCellIdentifier];
    }
    [self configureUsersCell:cell atIndexPath:indexPath];
    return cell;
     } else
     {
         GGSendCell* sendCell = [tableView dequeueReusableCellWithIdentifier:sendCellIdentifier];
         
         if (!sendCell) {
             sendCell = [[GGSendCell alloc]
                           initWithStyle:UITableViewCellStyleDefault
                           reuseIdentifier:sendCellIdentifier];
         }
         
         [self configureSendCell:sendCell atIndexPath:indexPath];
         return sendCell;

     }
}

#pragma mark - Cells configurations

-(void)configureSendCell:(GGSendCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    [cell.sendButton addTarget:self action:@selector(sendAction) forControlEvents:UIControlEventTouchUpInside];
}

-(void)configureUsersCell:(GGNotServiceUsersCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    GGNotSeviceUserModel* currUser = [self.sortedArray objectAtIndex:indexPath.row];
    cell.userNameLabel.text = currUser.userName;
    if([currUser.subsStatus isEqualToString:@"1"])
    {
        [cell.addButton setImage:[UIImage imageNamed:@"minus"] forState:UIControlStateNormal];
        [cell.addButton addTarget:self action:@selector(addToListActionWithButton:) forControlEvents:UIControlEventTouchUpInside];
        cell.addButton.tag = indexPath.row;
        
    } else {
        [cell.addButton setImage:[UIImage imageNamed:@"plus"] forState:UIControlStateNormal];
        [cell.addButton addTarget:self action:@selector(addToListActionWithButton:) forControlEvents:UIControlEventTouchUpInside];
        cell.addButton.tag = indexPath.row;
    }
    if (![currUser.userPhotoURL isEqualToString:@""]) {
        NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", baseURL, currUser.userPhotoURL]];
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        UIImage *profileImage = [UIImage imageWithData:imageData];
        cell.userImageView.image = profileImage;
    } else {
        cell.userImageView.image = [UIImage imageNamed:@"нет фото"];
    }
    [self.cellsArray addObject:cell];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 63.f;
}



#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSPredicate *predicate = [NSPredicate
                              predicateWithFormat:@"SELF.userName contains[cd] %@",searchText];
    
    self.sortedArray = [self.usersArray filteredArrayUsingPredicate:predicate];
    [self.tableView reloadData];
    
    if([searchText isEqualToString:@""])
    {
        self.sortedArray = self.usersArray;
        [self.tableView reloadData];
    }
    
}

#pragma mark - IBActions

-(void)sendAction
{
    NSMutableString * result = [[NSMutableString alloc] init];
    for (NSString * obj in self.invitedArray)
    {
        if ([obj isEqual:[self.invitedArray lastObject]]) {
            [result appendString:[NSString stringWithFormat:@"%@", obj]];
        } else
        {
            [result appendString:[NSString stringWithFormat:@"%@,", obj]];
        }
    }
    [[GGApiManager sharedManager] sendInvitationWithSubscriptionID:result
                                                         onSuccess:^(NSString *response) {
                                                             [self.navigationController popViewControllerAnimated:YES];

    }
                                                         onFailure:^(NSError *error, NSInteger statusCode) {
                                                             
                                                         }];
}

-(void)addToListActionWithButton:(UIButton*)button
{
    GGNotSeviceUserModel* currUser = [self.sortedArray objectAtIndex:button.tag];
    if (![self.invitedArray containsObject:currUser.user_id]) {
        if(![currUser.subsStatus isEqualToString:@"1"])
        {
            [self.invitedArray addObject:currUser.user_id];
            GGNotServiceUsersCell* cell = [self.cellsArray objectAtIndex:button.tag];
            [cell.addButton setTintColor:[UIColor blueColor]];
        }
    } else {
        [self.invitedArray removeObject:currUser.user_id];
        GGNotServiceUsersCell* cell = [self.cellsArray objectAtIndex:button.tag];
        [cell.addButton setTintColor:[UIColor grayColor]];
    }
}

-(void)addButton
{
    if (!self.isClickedAllMembers)
    {
        for (GGNotSeviceUserModel* user in self.sortedArray)
        {
            if(![user.subsStatus isEqualToString:@"1"])
            {
                if(![self.invitedArray containsObject:user.user_id])
                {
                    [self.invitedArray addObject:user.user_id];
                }
            }
        }
        for (GGNotServiceUsersCell* cell in self.cellsArray) {
            if(![cell.addButton.imageView.image isEqual:[UIImage imageNamed:@"minus"]])
            {
                [cell.addButton setTintColor:[UIColor blueColor]];
            }
        }
        self.isClickedAllMembers = YES;
    } else {
        self.isClickedAllMembers = NO;
        [self.invitedArray removeAllObjects];
        for (GGNotServiceUsersCell* cell in self.cellsArray) {
            if(![cell.addButton.imageView.image isEqual:[UIImage imageNamed:@"minus"]])
            {
                [cell.addButton setTintColor:[UIColor grayColor]];
            }
        }
    }
    //  [self.navigationController popViewControllerAnimated:YES];
}

-(void)backButton
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
