//
//  GGSendCell.m
//  Мессенджер организаций
//
//  Created by Гусейн on 28.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "GGSendCell.h"

@implementation GGSendCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self roundMyView:self.sendButton borderRadius:8.f borderWidth:1.5 color:[UIColor blueColor]];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)roundMyView:(UIView*)view
       borderRadius:(CGFloat)radius
        borderWidth:(CGFloat)border
              color:(UIColor*)color
{
    CALayer *layer = [view layer];
    layer.masksToBounds = YES;
    layer.cornerRadius = radius;
    layer.borderWidth = border;
    layer.borderColor = color.CGColor;
}

@end
