//
//  GGCompanyTabController.m
//  Мессенджер организаций
//
//  Created by Гусейн on 20.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "GGCompanyTabController.h"

@interface GGCompanyTabController ()

@end

@implementation GGCompanyTabController

- (void)viewDidLoad {
    UIImage *settings = [UIImage imageNamed:@"settings@3x.png"];
    UIImage *sale = [UIImage imageNamed:@"sale@3x.png"];
    UIImage *message = [UIImage imageNamed:@"message@3x.png"];
    
    UITabBarItem *item0 = [self.tabBar.items objectAtIndex:0];
    UITabBarItem *item1 = [self.tabBar.items objectAtIndex:1];
    UITabBarItem *item2 = [self.tabBar.items objectAtIndex:2];

    item0.title = @"Наши скидки";
    item1.title = @"Диалоги";
    item2.title = @"Настройки";

    item0.image = sale;
    item1.image = message;
    item2.image = settings;

    [super viewDidLoad];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
