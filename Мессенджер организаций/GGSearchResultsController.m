//
//  GGSearchResultsController.m
//  Мессенджер организаций
//
//  Created by Гусейн on 21.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "GGSearchResultsController.h"
#import "GGCityModel.h"
#import "GGCountryModel.h"
#import "GGApiManager.h"

@interface GGSearchResultsController ()

@property (nonatomic, getter=isLoading) BOOL loading;
@property (strong, nonatomic) NSString* searchingText;
@end

@implementation GGSearchResultsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.resultsArray = [NSMutableArray array];
    self.tableView.backgroundColor = [UIColor clearColor];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ([self.selectedSearchingType isEqualToString:@"Country"]) {
        [self searchCountries:0 andCount:20];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource implementation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.resultsArray) {
        return self.resultsArray.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    if (self.resultsArray) {
        cell.textLabel.text = [[self.resultsArray objectAtIndex:indexPath.row] name];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self dismissViewControllerAnimated:YES completion:^{
        if ([self.selectedSearchingType isEqualToString:@"City"]) {
            GGCityModel *selectedObject = [self.resultsArray objectAtIndex:indexPath.row];
            if(_delegate && [_delegate respondsToSelector:@selector(searchControllerDidDissmissWithCity:)])
            {
                [_delegate searchControllerDidDissmissWithCity:selectedObject];
            }
        } else if([self.selectedSearchingType isEqualToString:@"Country"]) {
            GGCountryModel *selectedObject = [self.resultsArray objectAtIndex:indexPath.row];
            if(_delegate && [_delegate respondsToSelector:@selector(searchControllerDidDissmissWithCountry:)])
            {
                [_delegate searchControllerDidDissmissWithCountry:selectedObject];
            }
        }

    }];
}

#pragma mark - Get data from server

-(void)searchCountries:(NSInteger)offset andCount:(NSInteger)count
{
    [[GGApiManager sharedManager]
     getCountriesWithOffset:offset
     count:count
     onSuccess:^(id response) {
         NSArray *result = [response objectForKey:@"response"];
         [self.resultsArray removeAllObjects];
         for (NSDictionary *currentObject in result) {
             GGCountryModel *country = [[GGCountryModel alloc] initWithName:[currentObject objectForKey:@"title"] andID:[[currentObject objectForKey:@"cid"] integerValue]];
             [self.resultsArray addObject:country];
         }
         [self.tableView reloadData];
         
 
     }
     onFailure:^(NSError *error, NSInteger statusCode) {
         
     }];
}

-(void)searchCityWithText:(NSString*)text andOffset:(NSInteger)offset andCount:(NSInteger)count
{
    [[GGApiManager sharedManager] getCitiesWithText:text
                                             offset:offset
                                              count:count
                                          countryID:self.countryID
                                          onSuccess:^(id response) {
                                              NSArray *result = [response objectForKey:@"response"];
                                              for (NSDictionary *currentObject in result) {
                                                  GGCityModel *country = [[GGCityModel alloc] initWithName:[currentObject objectForKey:@"title"] andID:[[currentObject objectForKey:@"cid"] integerValue]];
                                                  [self.resultsArray addObject:country];
                                              }
                                              [self.tableView reloadData];
                                           }
                                          onFailure:^(NSError *error, NSInteger statusCode) {
                                              
                                          }];
    self.searchingText = text;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (!self.isLoading && [self.resultsArray count] != 0) {
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height - scrollView.contentSize.height / 3) {
            
            self.loading = YES;
            if ([self.selectedSearchingType isEqualToString:@"City"]) {
                [self searchCityWithText:self.searchingText
                               andOffset:self.resultsArray.count andCount:20];
            }
        }
    }
}

-(id) ObjectOrNull: (id) object
{
    return object ?: [NSNull null];
}


@end
