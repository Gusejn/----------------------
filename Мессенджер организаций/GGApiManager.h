//
//  GGApiManager.h
//  Мессенджер организаций
//
//  Created by Гусейн on 20.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "AppDelegate.h"
#import <AFNetworking/AFNetworking.h>


@interface GGApiManager : AFHTTPSessionManager

@property (strong,nonatomic) NSOperationQueue* httpQueue;

+(GGApiManager*) sharedManager;

#pragma mark - Authorization

-(void) authorizeWithEmail:(NSString*)Email
                      type:(NSString*)authType
                  password:(NSString*)password
                     token:(NSString*)token
                 onSuccess:(void(^)(NSString* status))success
                 onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

-(void) authorizeWithPhone:(NSString*)Phone
                      type:(NSString*)authType
                  password:(NSString*)password
                     token:(NSString*)token
                 onSuccess:(void(^)(NSString* status))success
                 onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

-(void) authorizeWithSocialID:(NSString*)Phone
                         type:(NSString*)authType
                     password:(NSString*)password
                        token:(NSString*)token
                    onSuccess:(void(^)(NSString* status))success
                    onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

#pragma mark  - Registration
-(void) registrateCompanyWithParams:(NSDictionary*)params
                    onSuccess:(void(^)(NSString* status))success
                    onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

-(void) registrateClientWithParams:(NSDictionary*)params
                          onSuccess:(void(^)(NSString* status))success
                          onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

#pragma mark - Get cities from VK

-(void)getCountriesWithOffset:(NSInteger)offset
                        count:(NSInteger)count
                    onSuccess:(void(^)(id response))success
                    onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

-(void)getCitiesWithText:(NSString*)text
                  offset:(NSInteger)offset
                   count:(NSInteger)count
               countryID:(NSInteger)countryID
               onSuccess:(void(^)(id response))success
               onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

#pragma mark - Exit 

-(void)exitWithSuccess:(void(^)(id response))success
             onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

#pragma mark - Get Users (for service) 

-(void)getServiceUsersWithSuccess:(void(^)(id response))success
                        onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

-(void)getAllUsersWithSuccess:(void(^)(id response))success
                   onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

#pragma mark Get User Info

-(void)getUserInfoWithSuccess:(void(^)(id response))success
                    onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

#pragma mark - Update
-(void)updateInformationWithParams:(NSDictionary*)params
                         onSuccess:(void(^)(id response))success
                         onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

-(void)uploadImageForUser:(NSData*)image
                onSuccess:(void(^)(NSString* response))success
                onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

#pragma mark - Working with invitations 

-(void)sendInvitationWithSubscriptionID:(NSMutableString*)ID
                              onSuccess:(void(^)(NSString* response))success
                              onFailure: (void(^)(NSError* error, NSInteger statusCode))failure;

@end
