//
//  GGCompanyDialogsController.m
//  Мессенджер организаций
//
//  Created by Гусейн on 20.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "GGCompanyDialogsController.h"
#import "GGApiManager.h"
#import "GGCompanyUsersController.h"

@interface GGCompanyDialogsController ()

@end

@implementation GGCompanyDialogsController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:77/255.f
                                                                             green:169/255.f
                                                                              blue:250/255.f
                                                                             alpha:1.0]];
    self.navigationController.navigationBar.translucent = NO;
}

- (IBAction)companyUsersTapped:(UIBarButtonItem *)sender {
    GGCompanyUsersController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GGCompanyUsersController"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
