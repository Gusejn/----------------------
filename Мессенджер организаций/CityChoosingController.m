//
//  CityChoosingController.m
//  Мессенджер организаций
//
//  Created by Гусейн on 21.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "CityChoosingController.h"
#import "GGSearchDelegate.h"
#import "GGSearchResultsController.h"

typedef enum
{
   country,
    city
}selectedType;

@interface CityChoosingController () <GGSearchDelegate, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITextField *countryTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (strong, nonatomic) GGSearchResultsController *searchVC;
@property (strong, nonatomic) UISearchController* searchController;

@property (assign, nonatomic) BOOL selectedType;
@property (assign, nonatomic) NSInteger countryID;
@property (assign, nonatomic) NSInteger cityID;

- (IBAction)countryDidTap:(UITextField *)sender;
- (IBAction)cityDidTap:(UITextField *)sender;

@end

@implementation CityChoosingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];

    UIBarButtonItem *editingButton = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(doneButton:)];
    self.navigationItem.rightBarButtonItem = editingButton;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:77/255.f
                                                                             green:169/255.f
                                                                              blue:250/255.f
                                                                             alpha:1.0]];
    self.navigationController.navigationBar.translucent = NO;
    [self.cityTextField setEnabled:NO];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
}

- (void)updateNavigationController {
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self updateNavigationController];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return NO;
}

- (IBAction)countryDidTap:(UITextField *)sender
{
    self.selectedType = country;
    [self setSearchControllerWithIdentifier:@"Country"]; 
}
- (IBAction)cityDidTap:(UITextField *)sender
{
    self.selectedType = city;
    [self setSearchControllerWithIdentifier:@"City"];

}

-(void) setSearchControllerWithIdentifier:(NSString*)identifier
{
    
    self.navigationController.navigationBar.translucent = YES;
    self.searchVC = [self.storyboard instantiateViewControllerWithIdentifier:@"GGSearchResultsController"];
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchVC];
    self.searchVC.delegate = self;
    self.searchVC.countryID = self.countryID;
    self.searchVC.selectedSearchingType = identifier;
    self.searchController.searchBar.tintColor = [UIColor blackColor];
    self.searchController.searchBar.delegate = self;
    [self presentViewController:self.searchController animated:YES completion:^{
        
    }];
}

-(void)searchControllerDidDissmissWithCountry:(GGCountryModel *)country
{
    self.navigationController.navigationBar.translucent = NO;
    self.countryTextField.text = country.name;
    NSLog(@"%li", (long)country.ID);
    self.countryID = country.ID;
    [self.cityTextField setEnabled:YES];
}

-(void)searchControllerDidDissmissWithCity:(GGCityModel *)city
{
    self.navigationController.navigationBar.translucent = NO;
    self.cityTextField.text = city.name;
    self.cityID = city.ID;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(self.selectedType == city)
    {
        [self.searchVC.resultsArray removeAllObjects];
        [self.searchVC searchCityWithText:searchText andOffset:0 andCount:20];
    }
    NSLog(@"%@", searchText);
}


#pragma mark - IBActions

- (IBAction)doneButton:(UIBarButtonItem *)sender
{
    NSDictionary* dictionary = @{@"countryText":self.countryTextField.text,
                                 @"CountryID":@(self.countryID),
                                 @"cityText":self.cityTextField.text,
                                 @"cityID":@(self.cityID)};
    
    [self.delegate cityForAccDidSelectedWithDictionary:dictionary];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
