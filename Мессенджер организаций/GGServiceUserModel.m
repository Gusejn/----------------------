//
//  GGServiceUserModel.m
//  Мессенджер организаций
//
//  Created by Гусейн on 27.07.16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "GGServiceUserModel.h"

@implementation GGServiceUserModel

-(id)initWithDictionary:(NSDictionary*) otherDictionary
{
    self = [super init];
    if (self) {
        self.user_id = [otherDictionary objectForKey:@"user_id"];
        self.userName = [otherDictionary objectForKey:@"user_name"];
        self.userPhotoURL = [otherDictionary objectForKey:@"user_photo_url"];
        self.subsDiscount = [otherDictionary objectForKey:@"subscription_discount"];
    }
    return self;
}
@end
