//
//  GGNotSeviceUserModel.h
//  
//
//  Created by Гусейн on 27.07.16.
//
//

#import <Foundation/Foundation.h>
#import "GGServiceUserModel.h"

@interface GGNotSeviceUserModel : GGServiceUserModel

@property (strong, nonatomic) NSString* subsStatus ;

-(id)initWithDictionary:(NSDictionary*) otherDictionary;

@end
