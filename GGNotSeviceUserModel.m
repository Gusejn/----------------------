//
//  GGNotSeviceUserModel.m
//  
//
//  Created by Гусейн on 27.07.16.
//
//

#import "GGNotSeviceUserModel.h"

@implementation GGNotSeviceUserModel

-(id)initWithDictionary:(NSDictionary*) otherDictionary
{
    self = [super init];
    if (self) {
        self.user_id = [otherDictionary objectForKey:@"user_id"];
        self.userName = [otherDictionary objectForKey:@"user_name"];
        self.userPhotoURL = [otherDictionary objectForKey:@"user_photo_url"];
        self.subsDiscount = [otherDictionary objectForKey:@"subscription_discount"];
        self.subsStatus = [otherDictionary objectForKey:@"subscription_status"];
    }
    return self;
}

@end
